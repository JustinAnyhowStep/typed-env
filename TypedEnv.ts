import * as env from "node-env-file";

export class TypedEnv {
    private constructor () {}
    /*
        You DO NOT have to call Load() before using TypedEnv!
        TypedEnv is just a safer wrapper for process.env
    */
    public static Load (envPath? : string, overwrite : boolean = true) {
        if (envPath != null) {
            env.call(undefined, envPath, { overwrite: overwrite });
        }
        if (process.env.LOG_PROCESS_ENV == "true") {
            console.log(process.env);
        }
    }

    public static TryGetString (name : string) : string|undefined {
        return process.env[name];
    }
    //NaN is not allowed, INF is not allowed
    public static TryGetNumber (name : string) : number|undefined {
        return TypedEnv.TryGet(name, (str) => {
            const num = parseFloat(str);
            if (isNaN(num) || !isFinite(num)) {
                throw new Error(`Expected a number for ${name}, received ${str}`);
            }
            return num;
        });
    }
    public static TryGetBoolean (name : string) : boolean|undefined {
        return TypedEnv.TryGet(name, (str) => {
            str = str.toLowerCase();
            return str == "true" || str == "1";
        });
    }
    public static TryGetStringArray (name : string) : string[]|undefined {
        return TypedEnv.TryGet(name, (str) => {
            const arr = str.split(",");
            for (let i=arr.length-1; i>=1; --i) {
                const cur = arr[i];
                const prv = arr[i-1];
                if (prv.endsWith("\\")) {
                    //They tried to escape the comma
                    arr[i-1] += `,${cur}`;
                    arr.splice(i, 1);
                }
            }
            return arr;
        });
    }

    public static GetString (name : string, defaultValue : string) : string {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetString);
    }
    public static GetNumber (name : string, defaultValue : number) : number {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetNumber);
    }
    public static GetBoolean (name : string, defaultValue : boolean) : boolean {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetBoolean);
    }
    public static GetStringArray (name : string, defaultValue : string[]) : string[] {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetStringArray);
    }

    public static GetStringOrError (name : string) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetString);
    }
    public static GetNumberOrError (name : string) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetNumber);
    }
    public static GetBooleanOrError (name : string) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetBoolean);
    }
    public static GetStringArrayOrError (name : string) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetStringArray);
    }

    private static TryGet<T>(name : string, callback : (str : string) => T|undefined) : T|undefined {
        const str = TypedEnv.TryGetString(name);
        if (str == null) {
            return undefined;
        }
        return callback(str);
    }
    private static Get<T> (name : string, defaultValue : T, callback : (name : string) => T|undefined) : T {
        const result = callback(name);
        return (result != null) ? result : defaultValue;
    }
    private static GetOrError<T> (name : string, callback : (name : string) => T|undefined) : T {
        const value = callback(name);
        if (value == null) {
            throw new Error(`Expected a value for ${name}`);
        }
        return value;
    }
}
