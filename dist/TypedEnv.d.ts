export declare class TypedEnv {
    private constructor();
    static Load(envPath?: string, overwrite?: boolean): void;
    static TryGetString(name: string): string | undefined;
    static TryGetNumber(name: string): number | undefined;
    static TryGetBoolean(name: string): boolean | undefined;
    static TryGetStringArray(name: string): string[] | undefined;
    static GetString(name: string, defaultValue: string): string;
    static GetNumber(name: string, defaultValue: number): number;
    static GetBoolean(name: string, defaultValue: boolean): boolean;
    static GetStringArray(name: string, defaultValue: string[]): string[];
    static GetStringOrError(name: string): string;
    static GetNumberOrError(name: string): number;
    static GetBooleanOrError(name: string): boolean;
    static GetStringArrayOrError(name: string): string[];
    private static TryGet;
    private static Get;
    private static GetOrError;
}
