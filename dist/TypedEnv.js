"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const env = require("node-env-file");
class TypedEnv {
    constructor() { }
    /*
        You DO NOT have to call Load() before using TypedEnv!
        TypedEnv is just a safer wrapper for process.env
    */
    static Load(envPath, overwrite = true) {
        if (envPath != null) {
            env.call(undefined, envPath, { overwrite: overwrite });
        }
        if (process.env.LOG_PROCESS_ENV == "true") {
            console.log(process.env);
        }
    }
    static TryGetString(name) {
        return process.env[name];
    }
    //NaN is not allowed, INF is not allowed
    static TryGetNumber(name) {
        return TypedEnv.TryGet(name, (str) => {
            const num = parseFloat(str);
            if (isNaN(num) || !isFinite(num)) {
                throw new Error(`Expected a number for ${name}, received ${str}`);
            }
            return num;
        });
    }
    static TryGetBoolean(name) {
        return TypedEnv.TryGet(name, (str) => {
            str = str.toLowerCase();
            return str == "true" || str == "1";
        });
    }
    static TryGetStringArray(name) {
        return TypedEnv.TryGet(name, (str) => {
            const arr = str.split(",");
            for (let i = arr.length - 1; i >= 1; --i) {
                const cur = arr[i];
                const prv = arr[i - 1];
                if (prv.endsWith("\\")) {
                    //They tried to escape the comma
                    arr[i - 1] += `,${cur}`;
                    arr.splice(i, 1);
                }
            }
            return arr;
        });
    }
    static GetString(name, defaultValue) {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetString);
    }
    static GetNumber(name, defaultValue) {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetNumber);
    }
    static GetBoolean(name, defaultValue) {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetBoolean);
    }
    static GetStringArray(name, defaultValue) {
        return TypedEnv.Get(name, defaultValue, TypedEnv.TryGetStringArray);
    }
    static GetStringOrError(name) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetString);
    }
    static GetNumberOrError(name) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetNumber);
    }
    static GetBooleanOrError(name) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetBoolean);
    }
    static GetStringArrayOrError(name) {
        return TypedEnv.GetOrError(name, TypedEnv.TryGetStringArray);
    }
    static TryGet(name, callback) {
        const str = TypedEnv.TryGetString(name);
        if (str == null) {
            return undefined;
        }
        return callback(str);
    }
    static Get(name, defaultValue, callback) {
        const result = callback(name);
        return (result != null) ? result : defaultValue;
    }
    static GetOrError(name, callback) {
        const value = callback(name);
        if (value == null) {
            throw new Error(`Expected a value for ${name}`);
        }
        return value;
    }
}
exports.TypedEnv = TypedEnv;
//# sourceMappingURL=TypedEnv.js.map