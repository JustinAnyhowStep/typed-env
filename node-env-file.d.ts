declare module "node-env-file" {
    function call (_dummy : undefined, path : string, options? : EnvOptions) : void;
    interface EnvOptions {
        verbose? : boolean, //true
        overwrite?: boolean, //true
        raise? : boolean, //false
        logger? : Console //console
    }
}
